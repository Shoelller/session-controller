const InputS1=document.getElementById("session1")
const InputS2=document.getElementById("session2")
const InputS3=document.getElementById("session3")
const InputS4=document.getElementById("session4")
const InputS5=document.getElementById("session5")
const PreviousDate=document.getElementById("previous-date")
const NextDate=document.getElementById("next-date")
const SaveBtn=document.getElementById("save")

let session1, session2, session3, session4, session5
let now=Date.now()
let days, hours, seconds, minutes

SaveBtn.addEventListener("click",function(){
    session1=new Date(InputS1.value)
    session2=new Date(InputS2.value)
    session3=new Date(InputS3.value)
    session4=new Date(InputS4.value)
    session5=new Date(InputS5.value)

    if(session1>=now){
        timeStamp(session1)
        if(days==0){
            if (hours==0){
                if (minutes==0) {
                    if (seconds==0) {
                        PreviousDate.textContent=`Seans şu anda başladı.`
                    }
                    else{
                        PreviousDate.textContent=`1. Seans'ın başlamasına ${seconds} saniye var.`
                    }
                }
                else{
                    PreviousDate.textContent=`1. Seans'ın başlamasına ${minutes} dakika, ${seconds} saniye var.`
                }
            }
            else{
                PreviousDate.textContent=`1. Seans'ın başlamasına ${hours} saat, ${minutes} dakika, ${seconds} saniye var.`
            }
        }
        else{
            PreviousDate.textContent=`1. Seans'ın başlamasına ${days} gün, ${hours} saat, ${minutes} dakika, ${seconds} saniye var.`
        }
    }
    else if(session1 < now && session2>=now){
        timeStamp(session1)
        days+=1
        if(days==0){
            if (hours==0){
                if (minutes==0) {
                    if (seconds==0) {
                        PreviousDate.textContent=`Seans bitti.`
                    }
                    else{
                        PreviousDate.textContent=`1. Seans ${Maht.abs(seconds)} saniye önce bitti.`
                    }
                }
                else{
                    PreviousDate.textContent=`1. Seans ${Math.abs(minutes)} dakika, ${Math.abs(seconds)} saniye önce bitti.`
                }
            }
            else{
                PreviousDate.textContent=`1. Seans ${Math.abs(hours)} saat, ${Math.abs(minutes)} dakika, ${Math.abs(seconds)} saniye önce bitti.`
            }
        }
        else{
            PreviousDate.textContent=`1. Seans ${Math.abs(days)} gün, ${Math.abs(hours)} saat, ${Math.abs(minutes)} dakika, ${Math.abs(seconds)} saniye önce bitti.`
        }
        timeStamp(session2)
        if(days==0){
            if (hours==0){
                if (minutes==0) {
                    if (seconds==0) {
                        NextDate.textContent=`Seans şu anda başladı.`
                    }
                    else{
                        NextDate.textContent=`2. Seans'ın başlamasına ${seconds} saniye var.`
                    }
                }
                else{
                    NextDate.textContent=`2. Seans'ın başlamasına ${minutes} dakika, ${seconds} saniye var.`
                }
            }
            else{
                NextDate.textContent=`2. Seans'ın başlamasına ${hours} saat, ${minutes} dakika, ${seconds} saniye var.`
            }
        }
        else{
            NextDate.textContent=`2. Seans'ın başlamasına ${days} gün, ${hours} saat, ${minutes} dakika, ${seconds} saniye var.`
        }
    }
    else if(session2 < now && session3>=now){
        timeStamp(session2)
        days+=1
        if(days==0){
            if (hours==0){
                if (minutes==0) {
                    if (seconds==0) {
                        PreviousDate.textContent=`Seans bitti.`
                    }
                    else{
                        PreviousDate.textContent=`2. Seans ${Maht.abs(seconds)} saniye önce bitti.`
                    }
                }
                else{
                    PreviousDate.textContent=`2. Seans ${Math.abs(minutes)} dakika, ${Math.abs(seconds)} saniye önce bitti.`
                }
            }
            else{
                PreviousDate.textContent=`2. Seans ${Math.abs(hours)} saat, ${Math.abs(minutes)} dakika, ${Math.abs(seconds)} saniye önce bitti.`
            }
        }
        else{
            PreviousDate.textContent=`2. Seans ${Math.abs(days)} gün, ${Math.abs(hours)} saat, ${Math.abs(minutes)} dakika, ${Math.abs(seconds)} saniye önce bitti.`
        }
    
        timeStamp(session3)
        if(days==0){
            if (hours==0){
                if (minutes==0) {
                    if (seconds==0) {
                        NextDate.textContent=`Seans şu anda başladı.`
                    }
                    else{
                        NextDate.textContent=`3. Seans'ın başlamasına ${seconds} saniye var.`
                    }
                }
                else{
                    NextDate.textContent=`3. Seans'ın başlamasına ${minutes} dakika, ${seconds} saniye var.`
                }
            }
            else{
                NextDate.textContent=`3. Seans'ın başlamasına ${hours} saat, ${minutes} dakika, ${seconds} saniye var.`
            }
        }
        else{
            NextDate.textContent=`3. Seans'ın başlamasına ${days} gün, ${hours} saat, ${minutes} dakika, ${seconds} saniye var.`
        }
    }
    else if(session3 < now && session4>=now){
        timeStamp(session3)
        days+=1
        if(days==0){
            if (hours==0){
                if (minutes==0) {
                    if (seconds==0) {
                        PreviousDate.textContent=`Seans bitti.`
                    }
                    else{
                        PreviousDate.textContent=`3. Seans ${Maht.abs(seconds)} saniye önce bitti.`
                    }
                }
                else{
                    PreviousDate.textContent=`3. Seans ${Math.abs(minutes)} dakika, ${Math.abs(seconds)} saniye önce bitti.`
                }
            }
            else{
                PreviousDate.textContent=`3. Seans ${Math.abs(hours)} saat, ${Math.abs(minutes)} dakika, ${Math.abs(seconds)} saniye önce bitti.`
            }
        }
        else{
            PreviousDate.textContent=`3. Seans ${Math.abs(days)} gün, ${Math.abs(hours)} saat, ${Math.abs(minutes)} dakika, ${Math.abs(seconds)} saniye önce bitti.`
        }
    
        timeStamp(session4)
        if(days==0){
            if (hours==0){
                if (minutes==0) {
                    if (seconds==0) {
                        NextDate.textContent=`Seans şu anda başladı.`
                    }
                    else{
                        NextDate.textContent=`4. Seans'ın başlamasına ${seconds} saniye var.`
                    }
                }
                else{
                    NextDate.textContent=`4. Seans'ın başlamasına ${minutes} dakika, ${seconds} saniye var.`
                }
            }
            else{
                NextDate.textContent=`4. Seans'ın başlamasına ${hours} saat, ${minutes} dakika, ${seconds} saniye var.`
            }
        }
        else{
            NextDate.textContent=`4. Seans'ın başlamasına ${days} gün, ${hours} saat, ${minutes} dakika, ${seconds} saniye var.`
        }
    }
    else if(session4 < now && session5>=now){
        timeStamp(session4)
        days+=1
        if(days==0){
            if (hours==0){
                if (minutes==0) {
                    if (seconds==0) {
                        PreviousDate.textContent=`Seans bitti.`
                    }
                    else{
                        PreviousDate.textContent=`4. Seans ${Maht.abs(seconds)} saniye önce bitti.`
                    }
                }
                else{
                    PreviousDate.textContent=`4. Seans ${Math.abs(minutes)} dakika, ${Math.abs(seconds)} saniye önce bitti.`
                }
            }
            else{
                PreviousDate.textContent=`4. Seans ${Math.abs(hours)} saat, ${Math.abs(minutes)} dakika, ${Math.abs(seconds)} saniye önce bitti.`
            }
        }
        else{
            PreviousDate.textContent=`4. Seans ${Math.abs(days)} gün, ${Math.abs(hours)} saat, ${Math.abs(minutes)} dakika, ${Math.abs(seconds)} saniye önce bitti.`
        }
    
        timeStamp(session5)
        if(days==0){
            if (hours==0){
                if (minutes==0) {
                    if (seconds==0) {
                        NextDate.textContent=`Seans şu anda başladı.`
                    }
                    else{
                        NextDate.textContent=`5. Seans'ın başlamasına ${seconds} saniye var.`
                    }
                }
                else{
                    NextDate.textContent=`5. Seans'ın başlamasına ${minutes} dakika, ${seconds} saniye var.`
                }
            }
            else{
                NextDate.textContent=`5. Seans'ın başlamasına ${hours} saat, ${minutes} dakika, ${seconds} saniye var.`
            }
        }
        else{
            NextDate.textContent=`5. Seans'ın başlamasına ${days} gün, ${hours} saat, ${minutes} dakika, ${seconds} saniye var.`
        }
    }
    else{
        console.log("Seanslar tamamlanmıştır.");
    }
    
})

function timeStamp(time){
    let subtract=time-now
    days=Math.floor((subtract/1000/60/60/24)%30)
    hours=Math.floor((subtract/1000/60/60)%24)
    minutes=Math.floor((subtract/1000/60)%60)
    seconds=Math.floor((subtract/1000)%60)
    //console.log(hours,minutes,seconds);

    
}


